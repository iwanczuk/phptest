<?php

error_reporting(E_ALL);

if (version_compare(phpversion(), '5.5.0', '<') === true) {
    if (PHP_SAPI == 'cli') {
        echo 'Unsupported PHP version.';
    } else {
        echo <<<HTML
<!doctype html>
<html>
<head><title>Unsupported PHP version.</title></head>
<body><h1>Unsupported PHP version.</h1></body>
</html>
HTML;
    }
    exit(1);
}

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    $errno = $errno & error_reporting();
    if ($errno == 0) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

spl_autoload_register(function($class) {
    $namespace = 'Phptest\\';
    $len = strlen($namespace);
    if (strncmp($namespace, $class, $len) !== 0) {
        return;
    }
    $file = __DIR__
          . DIRECTORY_SEPARATOR
          . 'src'
          . DIRECTORY_SEPARATOR
          . str_replace('\\', DIRECTORY_SEPARATOR, substr($class, $len))
          . '.php';
    if (file_exists($file)) {
        require_once $file;
    }
});
