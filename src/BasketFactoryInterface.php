<?php

namespace Phptest;

interface BasketFactoryInterface
{
    /**
     * @param int $capacity
     * @return Basket
     */
    public function create($capacity);
}
