<?php

namespace Phptest;

interface NumberGeneratorInterface
{
    /**
     * @param int $minimum
     * @param int $maximum
     * @param int $count
     * @return \Generator
     */
    public function getNumbers($minimum, $maximum, $count);
}
