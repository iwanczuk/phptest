<?php

namespace Phptest;

class NumberGenerator implements NumberGeneratorInterface
{
    static private $numbers;

    /**
     * @param int $minimum
     * @param int $maximum
     * @param int $count
     * @return \Generator
     */
    public function getNumbers($minimum, $maximum, $count)
    {
        if (!is_int($minimum)) {
            throw new \InvalidArgumentException('Invalid minimum parameter.');
        }
        if (!is_int($maximum)) {
            throw new \InvalidArgumentException('Invalid maximum parameter.');
        }
        if (!is_int($count)) {
            throw new \InvalidArgumentException('Invalid count parameter.');
        }
        if ($minimum > $maximum) {
            throw new \LogicException('Minimum value cannot be greater than maximum.');
        }
        if ($count < 1) {
            throw new \LogicException('Count must be greater than zero.');
        }
        if ($count > ($maximum - $minimum + 1)) {
            throw new \LogicException('Count cannot be greater than possible value range.');
        }
        if (is_null(static::$numbers)) {
            static::$numbers = range($minimum, $maximum);
        }
        if ($count == 1) {
            yield array_rand(static::$numbers, $count);
        } else {
            foreach (array_rand(static::$numbers, $count) as $number) {
                yield static::$numbers[$number];
            }
        }
    }
}
