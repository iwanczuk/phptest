<?php

namespace Phptest;

interface BallFactoryInterface
{
    /**
     * @param int $number
     * @return Ball
     */
    public function create($number);
}
