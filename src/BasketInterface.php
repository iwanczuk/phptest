<?php

namespace Phptest;

interface BasketInterface
{
    /**
     * @return int
     */
    public function getCapacity();

    /**
     * @return BallInterface[]
     */
    public function getBalls();

    /**
     * @param BallInterface $ball
     * @param BasketInterface
     */
    public function addBall(BallInterface $ball);

    /**
     * @return array
     */
    public function getBallNumbers();

    /**
     * @param int $number
     * @return bool
     */
    public function hasBallNumber($number);
}
