<?php

namespace Phptest;

class HttpApplication extends Application
{
    public function dispatch()
    {
        $owned = $this->collectBaskets(new OwnedPredicate());
        $exac = $this->collectBaskets(new ExacOnePredicate());
        $html = '';
        foreach ($this->getBaskets() as $number => $basket) {
            $html .= $this->renderBasket($number, $basket);
        }
        $html .= $this->renderUserBasket($this->getUserBasket());
        if ($owned) {
            $html .= $this->renderOwned($owned);
        }
        if ($exac) {
            $html .= $this->renderExac($exac);
        }
        echo $this->renderPage($html);
    }

    public function handleException(\Exception $e)
    {
        header('Status: 503 Service Unavailable', true, 503);
        echo 'There has been an error processing your request';
    }

    /**
     * @param int $number
     * @param BasketInterface $basket
     * @return string
     */
    public function renderBasket($number, BasketInterface $basket)
    {
        $html = <<<HTML
<div>Basket $number:
HTML;
        $html .= ' ' . implode(', ', $basket->getBallNumbers());
        $html .= <<<HTML
</div>
HTML;
        return $html;
    }

    /**
     * @param BasketInterface $basket
     * @return string
     */
    public function renderUserBasket(BasketInterface $basket)
    {
        $html = <<<HTML
<div>Users basket:
HTML;
        $html .= ' ' . implode(', ', $basket->getBallNumbers());
        $html .= <<<HTML
</div>
HTML;
        return $html;
    }

    /**
     * @param array $owned
     * @return string
     */
    public function renderOwned(array $owned)
    {
        $html = <<<HTML
<div>Baskets with balls owned by user:
HTML;
        $html .= ' ' . implode(', ', array_keys($owned));
        $html .= <<<HTML
</div>
HTML;
        return $html;
    }

    /**
     * @param array $exac
     * @return string
     */
    public function renderExac(array $exac)
    {
        $html = <<<HTML
<div>Baskets with only one ball owned by user:
HTML;
        $html .= ' ' . implode(', ', array_keys($exac));
        $html .= <<<HTML
</div>
HTML;
        return $html;
    }

    /**
     * @param string $body
     * @return string
     */
    public function renderPage($body)
    {
        $html = <<<HTML
<!doctype html>
<html>
<head><title></title></head>
<body>$body</body>
</html>
HTML;
        return $html;
    }
}
