<?php

namespace Phptest;

class Basket implements BasketInterface
{
    private $capacity;
    private $balls;

    /**
     * @param int $capacity
     */
    public function __construct($capacity)
    {
        if (!is_int($capacity)) {
            throw new \InvalidArgumentException('Invalid capacity parameter.');
        }
        if ($capacity < 1) {
            throw new \LogicException('Capacity must be greater than zero.');
        }
        $this->capacity = $capacity;
        $this->balls    = [];
    }

    /**
     * @return int
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * @return BallInterface[] array
     */
    public function getBalls()
    {
        return $this->balls;
    }

    /**
     * @param BallInterface $ball
     * @return Basket
     */
    public function addBall(BallInterface $ball)
    {
        if ($this->getCapacity() <= count($this->balls)) {
            throw new \RuntimeException('Basket is full.');
        }
        $this->balls[] = $ball;
        return $this;
    }

    /**
     * @return array
     */
    public function getBallNumbers()
    {
        $numbers = [];
        foreach ($this->balls as $ball) {
            $numbers[] = $ball->getNumber();
        }
        return $numbers;
    }

    /**
     * @param int $ball
     * @param bool
     */
    public function hasBallNumber($number)
    {
        if (!is_int($number)) {
            throw new \InvalidArgumentException('Invalid ball number.');
        }
        foreach ($this->balls as $ball) {
            if ($ball->getNumber() === $number) {
                return true;
            }
        }
        return false;
    }
}
