<?php

namespace Phptest;

class ConsoleApplication extends Application
{
    public function dispatch()
    {
        $owned = $this->collectBaskets(new OwnedPredicate());
        $exac = $this->collectBaskets(new ExacOnePredicate());
        foreach ($this->getBaskets() as $number => $basket) {
            echo 'Basket ' . $number . ': ' . implode(', ', $basket->getBallNumbers()) . PHP_EOL;
        }
        echo 'Users basket: ' . implode(', ', $this->getUserBasket()->getBallNumbers()) . PHP_EOL;
        if ($owned) {
            echo 'Baskets with balls owned by user: ' . implode(', ', array_keys($owned)) . PHP_EOL;
        }
        if ($exac) {
            echo 'Baskets with only one ball owned by user: ' . implode(', ', array_keys($exac)) . PHP_EOL;
        }
    }

    /**
     * @param \Exception $e
     */
    public function handleException(\Exception $e)
    {
        echo $e->getMessage();
    }
}
