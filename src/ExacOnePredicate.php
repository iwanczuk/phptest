<?php

namespace Phptest;

class ExacOnePredicate implements PredicateInterface
{
    /**
     * @param BasketInterface $needle
     * @param BasketInterface $haystack
     * @return bool
     */
    public function match(BasketInterface $needle, BasketInterface $haystack)
    {
        $found = false;
        foreach ($needle->getBalls() as $ball) {
            if ($haystack->hasBallNumber($ball->getNumber())) {
                if ($found) {
                    return false;
                }
                $found = true;
            }
        }
        return $found;
    }
}
