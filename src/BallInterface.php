<?php

namespace Phptest;

interface BallInterface
{
    /**
     * @return int
     */
    public function getNumber();
}
