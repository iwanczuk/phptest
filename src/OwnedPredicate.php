<?php

namespace Phptest;

class OwnedPredicate implements PredicateInterface
{
    /**
     * @param BasketInterface $needle
     * @param BasketInterface $haystack
     * @return bool
     */
    public function match(BasketInterface $needle, BasketInterface $haystack)
    {
        foreach ($needle->getBalls() as $ball) {
            if (!$haystack->hasBallNumber($ball->getNumber())) {
                return false;
            }
        }
        return true;
    }
}
