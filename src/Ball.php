<?php

namespace Phptest;

class Ball implements BallInterface
{
    private $number;

    /**
     * @param int $number
     */
    public function __construct($number)
    {
        if (!is_int($number)) {
            throw new \InvalidArgumentException('Invalid ball number.');
        }
        $this->number = $number;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }
}
