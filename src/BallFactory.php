<?php

namespace Phptest;

class BallFactory implements BallFactoryInterface
{
    /**
     * @param int $number
     * @return Ball
     */
    public function create($number)
    {
        return new Ball($number);
    }
}
