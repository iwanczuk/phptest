<?php

namespace Phptest;

interface PredicateInterface
{
    /**
     * @param BasketInterface $needle
     * @param BasketInterface $haystack
     * @return bool
     */
    public function match(BasketInterface $needle, BasketInterface $haystack);
}
