<?php

namespace Phptest;

class BasketFactory implements BasketFactoryInterface
{
    /**
     * @param int $capacity
     * @return Basket
     */
    public function create($capacity)
    {
        return new Basket($capacity);
    }
}
