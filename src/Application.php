<?php

namespace Phptest;

abstract class Application
{
    const BACKET_COUNT         = 30;
    const BASKET_CAPACITY      = 10;
    const USER_BASKET_CAPACITY = 100;

    const MINIMUM_NUMBER = 1;
    const MAXIMUM_NUMBER = 999;

    private $numberGenerator;
    private $basketFactory;
    private $ballFactory;
    private $baskets;
    private $userBasket;

    /**
     * @param NumberGeneratorInterface $numberGenerator
     * @param BasketFactoryInterface $basketFactory
     * @param BallFactoryInterface $ballFactory
     */
    public function __construct(
        NumberGeneratorInterface $numberGenerator,
        BasketFactoryInterface $basketFactory,
        BallFactoryInterface $ballFactory
    ) {
        $this->numberGenerator = $numberGenerator;
        $this->basketFactory   = $basketFactory;
        $this->ballFactory     = $ballFactory;
        $this->baskets         = [];
        $this->userBasket      = null;
    }

    /**
     * @param int $basketCount
     * @param int $basketCapacity
     * @param int $userCapacity
     * @return Application
     */
    public static function init(
        $basketCount = self::BACKET_COUNT,
        $basketCapacity = self::BASKET_CAPACITY,
        $userCapacity = self::USER_BASKET_CAPACITY
    ) {
        if (!is_int($basketCount)) {
            throw new \InvalidArgumentException('Invalid basketCount parameter.');
        }
        if (!is_int($basketCapacity)) {
            throw new \InvalidArgumentException('Invalid basketCapacity parameter.');
        }
        if (!is_int($userCapacity)) {
            throw new \InvalidArgumentException('Invalid userCapacity parameter.');
        }
        if ($basketCount < 1) {
            throw new \LogicException('basketCount must be greater than zero.');
        }
        if ($basketCapacity < 1) {
            throw new \LogicException('basketCapacity must be greater than zero.');
        }
        if ($userCapacity < 1) {
            throw new \LogicException('userCapacity must be greater than zero.');
        }
        $application = new static(new NumberGenerator(), new BasketFactory(), new BallFactory());
        for ($i = 0; $i < $basketCount; $i++) {
            $basket = $application->createBasket($basketCapacity);
            $application->fillBasket($basket);
            $application->addBasket($basket);
        }
        $userBasket = $application->createBasket($userCapacity);
        $application->fillBasket($userBasket);
        $application->setUserBasket($userBasket);
        return $application;
    }

    public abstract function dispatch();

    public abstract function handleException(\Exception $e);

    public function run()
    {
        try {
            $this->dispatch();
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * @param int $capacity
     * @return BasketInterface
     */
    public function createBasket($capacity)
    {
        return $this->basketFactory->create($capacity);
    }

    /**
     * @param int $number
     * @return BallInterface
     */
    public function createBall($number)
    {
        return $this->ballFactory->create($number);
    }

    /**
     * @param BasketInterface $basket
     * @return Application
     */
    public function addBasket(BasketInterface $basket)
    {
        $this->baskets[] = $basket;
        return $this;
    }

    /**
     * @return BasketInterface[]
     */
    public function getBaskets()
    {
        return $this->baskets;
    }

    /**
     * @return BasketInterface
     */
    public function getUserBasket()
    {
        return $this->userBasket;
    }

    /**
     * @param BasketInterface $basket
     * @return Application
     */
    public function setUserBasket(BasketInterface $basket)
    {
        $this->userBasket = $basket;
        return $this;
    }

    /**
     * @param PredicateInterface $predicate
     * @return BasketInterface[]
     */
    public function collectBaskets(PredicateInterface $predicate)
    {
        $baskets = [];
        foreach ($this->baskets as $number => $basket) {
            if ($predicate->match($basket, $this->userBasket)) {
                $baskets[$number] = $basket;
            }
        }
        return $baskets;
    }

    /**
     * @param BasketInterface $basket
     * @return BasketInterface
     */
    public function fillBasket(BasketInterface $basket)
    {
        foreach (
            $this->numberGenerator->getNumbers(
                self::MINIMUM_NUMBER,
                self::MAXIMUM_NUMBER,
                mt_rand(1, $basket->getCapacity())
            ) as $number
        ) {
            $basket->addBall($this->createBall($number));
        }
        return $basket;
    }
}
