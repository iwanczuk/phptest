#!/usr/bin/env php
<?php

require_once __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';

if (PHP_SAPI != 'cli') {
    exit;
}

\Phptest\ConsoleApplication::init()->run();

exit;