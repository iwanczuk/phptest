<?php

namespace Phptest\Tests;

use Phptest\Basket;
use Phptest\Ball;

class BasketTest extends \PHPUnit_Framework_TestCase
{
    public function testCapacity()
    {
        $basket = new Basket(123);
        $this->assertEquals($basket->getCapacity(), 123);
    }

    public function testInvalidCapacity()
    {
        $this->setExpectedException('InvalidArgumentException');
        $basket = new Basket('123');
    }

    public function testZeroOrNegativeCapacity()
    {
        $this->setExpectedException('LogicException');
        $basket = new Basket(0);
    }

    public function testEmptyBalls()
    {
        $basket = new Basket(1);
        $this->assertEmpty($basket->getBalls());
    }

    public function testAddBalls()
    {
        $basket = new Basket(1);
        $ball   = new Ball(1);
        $basket->addBall($ball);
        $this->assertContains($ball, $basket->getBalls());
    }

    public function testAddBallsOverCapacity()
    {
        $this->setExpectedException('RuntimeException');
        $basket = new Basket(1);
        $basket->addBall(new Ball(1));
        $basket->addBall(new Ball(2));
    }

    public function testHasBallNumber()
    {
        $basket = new Basket(1);
        $basket->addBall(new Ball(1));
        $this->assertTrue($basket->hasBallNumber(1));
    }

    public function testHasNotBallNumber()
    {
        $basket = new Basket(1);
        $basket->addBall(new Ball(1));
        $this->assertFalse($basket->hasBallNumber(2));
    }
}
