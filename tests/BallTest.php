<?php

namespace Phptest\Tests;

use Phptest\Ball;

class BallTest extends \PHPUnit_Framework_TestCase
{
    public function testNumber()
    {
        $ball = new Ball(123);
        $this->assertEquals($ball->getNumber(), 123);
    }

    public function testInvalidNumber()
    {
        $this->setExpectedException('InvalidArgumentException');
        $ball = new Ball('123');
    }
}
