<?php

namespace Phptest\Tests;

use Phptest\NumberGenerator;

class NumberGeneratorTest extends \PHPUnit_Framework_TestCase
{
    public function testInvalidMinimum()
    {
        $this->setExpectedException('InvalidArgumentException');
        $generator = new NumberGenerator();
        foreach ($generator->getNumbers('1', 1, 1) as $number) {}
    }

    public function testInvalidMaximum()
    {
        $this->setExpectedException('InvalidArgumentException');
        $generator = new NumberGenerator();
        foreach ($generator->getNumbers(1, '1', 1) as $number) {}
    }

    public function testInvalidCount()
    {
        $this->setExpectedException('InvalidArgumentException');
        $generator = new NumberGenerator();
        foreach ($generator->getNumbers(1, 1, '1') as $number) {}
    }

    public function testMinimumGreaterThanMaximum()
    {
        $this->setExpectedException('LogicException');
        $generator = new NumberGenerator();
        foreach ($generator->getNumbers(2, 1, 1) as $number) {}
    }

    public function testCountRange()
    {
        $this->setExpectedException('LogicException');
        $generator = new NumberGenerator();
        foreach ($generator->getNumbers(1, 1, 2) as $number) {}
    }

    public function testValues()
    {
        $generator = new NumberGenerator();
        $numbers = [];
        foreach ($generator->getNumbers(1, 3, 3) as $number) {
            $numbers[] = $number;
        }
        $this->assertContains(1, $numbers);
        $this->assertContains(2, $numbers);
        $this->assertContains(3, $numbers);
    }
}
