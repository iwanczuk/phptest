<?php

namespace Phptest\Tests;

use Phptest\Ball;
use Phptest\Basket;
use Phptest\ExacOnePredicate;

class ExacOnePredicateTest extends \PHPUnit_Framework_TestCase
{
    public function testMatch()
    {
        $basket = new Basket(10);
        foreach ([11, 290, 456] as $number) {
            $basket->addBall(new Ball($number));
        }

        $basket2 = new Basket(10);
        foreach ([78, 235, 987] as $number) {
            $basket2->addBall(new Ball($number));
        }

        $basket3 = new Basket(10);
        foreach ([36, 78, 134] as $number) {
            $basket3->addBall(new Ball($number));
        }

        $userBasket = new Basket(100);
        foreach ([1, 3, 9, 36, 78, 134, 256, 290, 321, 332, 415, 550] as $number) {
            $userBasket->addBall(new Ball($number));
        }

        $predicate = new ExacOnePredicate();
        $this->assertTrue($predicate->match($basket, $userBasket));
        $this->assertTrue($predicate->match($basket2, $userBasket));
        $this->assertFalse($predicate->match($basket3, $userBasket));
    }
}
